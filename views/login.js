const e = React.createElement;

class Login extends React.Component {
   constructor(props) {
       super(props);
   }

   render() {
       return (
        <div class="card mt-4" Style="width: 100%;">
          <div div class="card-body">
            <form action="/login" method="post">
              <div class="form-row">
                <div class="col">
                  <input type="text" class="form-control" name="username" placeholder="slash" value="slash"/>
                </div>
                <div class="col">
                  <input type="password" class="form-control" name="password" placeholder="slash1234" value="slash1234"/>
                </div>
                <div className="col">
                  <button type="submit" class="btn btn-primary">Login</button>
                </div>
              </div>
            </form>
          </div>
        </div>
       );
   }
}

const domContainer = document.querySelector('#root');
ReactDOM.render(e(Login), domContainer);